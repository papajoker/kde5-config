### but
Espionner les configurations de

    -KDE 5 brute fraichement instalée (plasma-meta dolphin kate konsole sddm )
    -et quelques logiciels...

>git init du dossier `~/.config/`


une modification de la configuration
voir les fichiers modifiers et les commiter

    # kate & (change...)
    git status       # quel fichier(s) a été modifié
    git add .        # ajouter ces fichiers
                     # mettre un descriptif
    git commit -m "réglage de la taille de la police par defaut de kate"
    
une fois les 36 changements finis, envoyer tout les commits sur gitlab histoire d'avoir une belle interface
